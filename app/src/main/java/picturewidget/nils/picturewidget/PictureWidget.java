package picturewidget.nils.picturewidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.util.Calendar;


/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link PictureWidgetConfigureActivity PictureWidgetConfigureActivity}
 */
public class PictureWidget extends AppWidgetProvider {

    private static final String CLICK = "CLICK";

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        // Construct the RemoteViews object
        try {
            //Toast.makeText(context, "Updating...",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, PictureWidget.class);
            intent.setAction(CLICK);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, appWidgetId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.picture_widget);
            views.setOnClickPendingIntent(R.id.imageView, pendingIntent);
            views.setTextViewText(R.id.textView2, "Updating...");
            CharSequence widgetUrl = PictureWidgetConfigureActivity.loadUrlPref(context, appWidgetId);
            AsyncTask task = new DownloadImageTask(views, appWidgetManager, appWidgetId, context).execute(widgetUrl);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }catch (Exception e){
            Toast.makeText(context, e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        try{
            AppWidgetManager manager = AppWidgetManager.getInstance(context);
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.picture_widget);
            if (intent.getAction().equals(CLICK)) {
                Bundle extras = intent.getExtras();
                int widgetId = 0;
                ComponentName thisWidget = new ComponentName(context,PictureWidget.class);
                widgetId = intent.getIntExtra(manager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
                if(widgetId != AppWidgetManager.INVALID_APPWIDGET_ID)
                    updateAppWidget(context, manager, widgetId);
            }
        }catch (Exception e){
            Toast.makeText(context, e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }
    private static class DownloadImageTask extends AsyncTask {
        public DownloadImageTask(RemoteViews v, AppWidgetManager mgr, int id, Context c)
        {
            _views = v;
            _mgr = mgr;
            _appWidgetId = id;
            _ctxt = c;
        }
        RemoteViews _views;
        Context _ctxt;
        AppWidgetManager _mgr;
        int _appWidgetId;
        @Override
        protected Bitmap doInBackground(Object[] urls) {
            try {
                URL url = new URL((String) urls[0]);
                URLConnection uc = url.openConnection();
                boolean auth = PictureWidgetConfigureActivity.loadAuthPref(_ctxt, _appWidgetId);
                if(auth){
                    String authString = PictureWidgetConfigureActivity.loadAuthStringPref(_ctxt, _appWidgetId);
                    uc.setRequestProperty("Authorization", authString);
                }
                return BitmapFactory.decodeStream(uc.getInputStream());
            }catch(Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            try {
                _views.setImageViewBitmap(R.id.imageView, (Bitmap) result);
                Boolean showTimestamp = PictureWidgetConfigureActivity.loadShowTimestampPref(_ctxt, _appWidgetId);
                if (showTimestamp) {
                    _views.setTextViewText(R.id.textView2, DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime()));
                } else {
                    _views.setTextViewText(R.id.textView2, "");
                }
                _mgr.updateAppWidget(_appWidgetId, _views);
            }catch(Exception e){
                Toast.makeText(_ctxt, e.getMessage(),Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            PictureWidgetConfigureActivity.deleteUrlPref(context, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        //Toast.makeText(context, "ENABLE",Toast.LENGTH_LONG).show();
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.picture_widget);
        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        ComponentName thisWidget = new ComponentName(context,PictureWidget.class);
        manager.updateAppWidget(thisWidget, views);
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

