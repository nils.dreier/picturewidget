package picturewidget.nils.picturewidget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;

import java.util.Base64;

/**
 * The configuration screen for the {@link PictureWidget PictureWidget} AppWidget.
 */
public class PictureWidgetConfigureActivity extends Activity {

    private static final String PREFS_NAME = "picturewidget.nils.picturewidget.PictureWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    EditText mAppWidgetUrlText;
    Switch mAppWidgetShowTimestampSwitch;
    Switch mAppWidgetAuthSwitch;
    EditText mAppWidgetUsernameText;
    EditText mAppWidgetPasswordText;
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                final Context context = PictureWidgetConfigureActivity.this;
                SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();

                // When the button is clicked, store the string locally
                String url = mAppWidgetUrlText.getText().toString();
                prefs.putString(PREF_PREFIX_KEY + mAppWidgetId + "_url", url);
                boolean showTimestamp = mAppWidgetShowTimestampSwitch.isChecked();
                prefs.putBoolean(PREF_PREFIX_KEY + mAppWidgetId + "_showtimestamp", showTimestamp);
                boolean auth = mAppWidgetAuthSwitch.isChecked();
                prefs.putBoolean(PREF_PREFIX_KEY + mAppWidgetId + "_auth", auth);
                if(auth) {
                    String username = mAppWidgetUsernameText.getText().toString();
                    String password = mAppWidgetPasswordText.getText().toString();
                    String userpass = username + ":" + password;
                    String authstring = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
                    prefs.putString(PREF_PREFIX_KEY + mAppWidgetId + "_authString", authstring);
                }


                prefs.apply();
                // It is the responsibility of the configuration activity to update the app widget
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                PictureWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

                // Make sure we pass back the original appWidgetId
                Intent resultValue = new Intent();
                resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                setResult(RESULT_OK, resultValue);
                finish();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    };

    public PictureWidgetConfigureActivity() {
        super();
    }

    // Read the prefix from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default from a resource
    static String loadUrlPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString(PREF_PREFIX_KEY + appWidgetId + "_url", null);
    }

    static Boolean loadShowTimestampPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getBoolean(PREF_PREFIX_KEY + appWidgetId + "_showtimestamp", false);
    }

    static Boolean loadAuthPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getBoolean(PREF_PREFIX_KEY + appWidgetId + "_auth", false);
    }

    static String loadAuthStringPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        return prefs.getString(PREF_PREFIX_KEY + appWidgetId + "_authString", null);
    }

    static void deleteUrlPref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + appWidgetId);
        prefs.apply();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);

        setContentView(R.layout.picture_widget_configure);
        mAppWidgetUrlText = (EditText) findViewById(R.id.appwidget_text);
        findViewById(R.id.add_button).setOnClickListener(mOnClickListener);
        mAppWidgetShowTimestampSwitch = (Switch) findViewById(R.id.showDateSwitch);
        mAppWidgetAuthSwitch = (Switch) findViewById(R.id.AuthSwitch);
        mAppWidgetUsernameText = (EditText) findViewById(R.id.username_edit);
        mAppWidgetPasswordText = (EditText) findViewById(R.id.password_edit);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }
    }
}

